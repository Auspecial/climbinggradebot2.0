import praw
import os
from prawcore.exceptions import PrawcoreException

reddit = praw.Reddit('bot1')

USA = ['5.8', '5.9', '5.10a', '5.10b', '5.10c', '5.10d', '5.11a', '5.11b', '5.11c', '5.11d', '5.12a', '5.12b', '5.12c', '5.12d', '5.13a', '5.13b', '5.13c', '5.13d', '5.14a', '5.14b', '5.14c', '5.14d', '5.15a', '5.15b', '5.15c']
French = ['5b', '5c', '6a', '6a+', '6b', '6b+', '6c', '6c/6c+', '6c+', '7a', '7a+', '7b', '7b+', '7c', '7c+', '8a', '8a+', '8b', '8b+', '8c', '8c+', '9a', '9a+', '9b', '9b+']
Ewbank = ['16', '17', '18', '19', '20', '20/21', '21', '22', '23', '24', '25', '26', '27', '28', '29', '29/30', '30', '31', '32', '33', '34', '35', '36', '37', '38']
UIAA = ['v+', 'vi-', 'vi', 'vi+', 'vii-', 'vii', 'vii', 'vii+', 'viii-', 'viii', 'viii+', 'viii+', 'ix-', 'ix', 'ix+', 'ix+/x-', 'x-/x', 'x/x+', 'x+', 'xi-', 'xi-/xi', 'xi/xi+', 'xi+', 'xii-', 'xiii']
Vgrades = ['v0', 'v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'v7', 'v8', 'v9', 'v10', 'v11', 'v12', 'v13', 'v14', 'v15', 'v16', 'v17']
Font = ['4', '5', '5+', '6a/6a+', '6b/6b+', '6c/6c+', '7a', '7a+', '7b/7b+', '7b+/7c', '7c+', '8a', '8a+', '8b', '8b+', '8c', '8c+', '9a']
print(len(Vgrades), len(Font))

if not os.path.isfile("posts_replied_to.txt"):
    posts_replied_to = []
else:
    with open("posts_replied_to.txt", "r") as f:
        posts_replied_to = f.read()
        posts_replied_to = posts_replied_to.split('\n')
        posts_replied_to = list(filter(None, posts_replied_to))


def findwordSport(com):
    words = com.split(" ")
    lword = [word.lower() for word in words]
    for idx, grade in enumerate(USA):
        if grade in lword:
            return idx
    for idx, grade in enumerate(French):
        if grade in lword:
            return idx
    else:
        return -1


def findwordBoulder(com):
    words = com.split(" ")
    lword = [word.lower() for word in words]
    for idx, grade in enumerate(Vgrades):
        if grade in lword:
            return idx
    else:
        return -1


subreddit = reddit.subreddit('climbing')
try:
    for comment in subreddit.stream.comments():
        print(comment.body)
        # makes sure the bot didnt post the comment and that it hasnt already replied to this comment
        if comment.author.name != 'WhatGradeIsThat' and comment.id not in posts_replied_to:
            body = comment.body
            index = findwordSport(body)
            if index >= 0:
                print('---------------------------')
                print("replying to " + comment.id)
                comment.reply("**Grading System** | **Grade** \n" + ":------------: | :------: \n" +
                              "YDS | " + USA[index].upper() + "\n" +
                              "French | " + French[index].upper() + "\n" +
                              "Aus/Ewbank | " + Ewbank[index].upper() + "\n" +
                              "UIAA | " + UIAA[index].upper() + "\n \n" +
                              "Grades are subjective and hard to compare for more information see "
                              "[here](https://www.thecrag.com/en/article/grades). \n \n "
                              "Do you think this bot can be better? "
                              "[Please message me!](https://www.reddit.com/message/compose/?to=WhatGradeIsThat)")
                posts_replied_to.append(comment.id)
                with open("posts_replied_to.txt", 'a') as f:
                    f.write(comment.id + '\n')
            elif index == -1:
                bindex = findwordBoulder(body)
                if bindex >= 0:
                    print('---------------------------')
                    print("replying to " + comment.id)
                    comment.reply("**Grading System** | **Grade** \n" + ":------------: | :------: \n" +
                                  "V Grade | " + Vgrades[bindex].upper() + "\n" +
                                  "Font Grade | " + Font[bindex].upper() + "\n \n" +
                                  "Grades are subjective and hard to compare for more information see "
                                  "[here](https://www.thecrag.com/en/article/grades). \n \n "
                                  "Do you think this bot can be better? "
                                  "[Please message me!](https://www.reddit.com/message/compose/?to=WhatGradeIsThat)")
                    posts_replied_to.append(comment.id)
                    with open("posts_replied_to.txt", 'a') as f:
                        f.write(comment.id + '\n')
except PrawcoreException:
    print('praw exception occurred')
except KeyboardInterrupt:
    print('bot stopped, goodbye.')
